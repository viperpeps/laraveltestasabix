<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\PostTranslation;
use App\Models\Tag;
use App\Repositories\LanguageRepository;
use App\Repositories\PostRepository;
use App\Repositories\PostTranslationRepository;
use App\Services\LanguageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


class PostController extends Controller
{

    public function __construct()
    {
        LanguageService::checkAvailableLanguage();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(PostRepository $postRepository)
    {
        $posts = $postRepository->getAllByLanguage(App::getLocale());
        return response()->json($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request, LanguageRepository $languageRepository)
    {
        // Validate the forms
//        $validated = $request->validate([
//            'title' => 'required|max:255',
//            'description' => 'required',
//            'content_value' => 'required',
//            'language' => 'required',
//            'tag' => 'required'
//        ]);

        DB::beginTransaction();
        try {
            $newPost = new Post();
            $newPost->created_at = NOW();
            $newPost->updated_at = NOW();
            $newPost->save();

            $newPostTranslation = new PostTranslation();
            $newPostTranslation->title = $request->title;
            $newPostTranslation->description = $request->description;
            $newPostTranslation->content = $request->content_value;
            $newPostTranslation->post_id = $newPost->id;
            $newPostTranslation->language_id = $languageRepository->getLanguageByPrefix(App::getLocale())->id;
            $newPostTranslation->save();

            $newPostTags = new PostTag();
            $newPostTags->post_id = $newPost->id;
            $newPostTags->tag_id = Tag::find($request->tag)->id;
            $newPostTags->save();

            DB::commit();
            return response()->json([
                'message' => 'Post created',
                'code' => 200,
                'error' => false,
                'results' => $newPostTranslation
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'error' => true,
                'code' => 500
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($language, $id, PostRepository $postRepository)
    {
        $post = $postRepository->getOnePostByIdAndLanguage($id, App::getLocale());
        return response()->json($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, PostTranslationRepository $translationRepository, $language, $id)
    {
        DB::beginTransaction();
        try {
            $postTranslation = $translationRepository->getOnePostByIdAndLanguage($id, App::getLocale());
            $postTranslation->title = $request->title;
            $postTranslation->description = $request->description;
            $postTranslation->content = $request->content_value;
            $postTranslation->save();

            $post = Post::find($postTranslation->id);
            $post->updated_at = NOW();
            $post->save();

            DB::commit();
            return response()->json([
                'message' => 'Post updated',
                'code' => 200,
                'error' => false,
                'results' => $postTranslation
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'error' => true,
                'code' => 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($language, $id)
    {
        $post = Post::find($id);
        try {
            if ($post) {
                $post->delete();
                return response()->json([
                    'message' => 'Deleted',
                    'code' => 200,
                    'error' => false,
                    'results' => null
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'error' => true,
                'code' => 500
            ], 500);
        }
    }
}
