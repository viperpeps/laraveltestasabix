<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Repositories\TagRepository;
use App\Services\LanguageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{

    public function __construct()
    {
        LanguageService::checkAvailableLanguage();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(TagRepository $tagRepository)
    {
        return response()->json($tagRepository->getAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $newTag= new Tag();
            $newTag->name = $request->name;
            $newTag->created_at = NOW();
            $newTag->updated_at = NOW();
            $newTag->save();
            return response()->json([
                'message' => 'Tag created',
                'code' => 200,
                'error' => false,
                'results' => $newTag
            ], 201);
        } catch(\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'error' => true,
                'code' => 500
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($language, int $id, TagRepository $tagRepository): JsonResponse
    {
        return response()->json($tagRepository->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $language, $id, TagRepository $tagRepository)
    {
        try {
            $tag= $tagRepository->getById($id);
            $tag->name = $request->name;
            $tag->updated_at = NOW();
            $tag->save();
            return response()->json([
                'message' => 'Update created',
                'code' => 200,
                'error' => false,
                'results' => $tag
            ], 201);
        } catch(\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'error' => true,
                'code' => 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($language, $id)
    {
        $tag = Tag::find($id);
        if($tag){
            $tag->delete();
            return response()->json([
                'message' => 'Deleted',
                'code' => 200,
                'error' => false,
                'results' => null
            ], 201);
        }
        return response()->json([
            'message' => 'Error',
            'error' => true,
            'code' => 500
        ], 500);
    }
}
