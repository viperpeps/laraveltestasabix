<?php

namespace App\Repositories;

use App\Models\Language;

class LanguageRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Language::class;
    }

    public function getLanguageByPrefix($language)
    {
        return $this->startConditions()
            ->where('languages.prefix', $language)
            ->first();
    }
}
