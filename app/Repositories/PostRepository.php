<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Post::class;
    }

    public function getAllByLanguage($language)
    {
        return $this->startConditions()
            ->select('posts.id', 'post_translations.title', 'post_translations.description', 'post_translations.content',
                'languages.locale', 'posts.created_at', 'posts.updated_at')
            ->leftJoin('post_translations', 'posts.id', '=', 'post_translations.post_id')
            ->leftJoin('languages', 'languages.id', '=', 'post_translations.language_id')
            ->where('languages.prefix', $language)
            ->whereNull('posts.deleted_at')
            ->paginate(10);
    }

    public function getOnePostByIdAndLanguage($id, $language)
    {
        return $this->startConditions()
            ->select('posts.id', 'post_translations.title', 'post_translations.description', 'post_translations.content',
                'languages.locale', 'posts.created_at', 'posts.updated_at')
            ->leftJoin('post_translations', 'posts.id', '=', 'post_translations.post_id')
            ->leftJoin('languages', 'languages.id', '=', 'post_translations.language_id')
            ->where('languages.prefix', $language)
            ->where('posts.id', $id)
            ->whereNull('posts.deleted_at')
            ->first();
    }
}
