<?php

namespace App\Repositories;

use App\Models\PostTranslation;

class PostTranslationRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return PostTranslation::class;
    }

    public function getOnePostByIdAndLanguage($id, $language)
    {
        return $this->startConditions()
            ->select('posts.id', 'post_translations.title', 'post_translations.description', 'post_translations.content',
                'languages.locale', 'posts.created_at', 'posts.updated_at')
            ->leftJoin('posts', 'posts.id', '=', 'post_translations.post_id')
            ->leftJoin('languages', 'languages.id', '=', 'post_translations.language_id')
            ->where('languages.prefix', $language)
            ->where('posts.id', $id)
            ->whereNull('posts.deleted_at')
            ->first();
    }
}
