<?php

namespace App\Repositories;

use App\Models\Tag;

class TagRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Tag::class;
    }

    public function softDelete()
    {
        return $this->startConditions()->whereNull('deleted_at');
    }

    public function getAll()
    {
        return $this->softDelete()->paginate();
    }

    public function getById($id)
    {
        return $this->softDelete()->where('id', $id)->first();
    }
}
