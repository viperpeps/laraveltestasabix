<?php

namespace App\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

class LanguageService
{
    public static function checkAvailableLanguage()
    {
        if ($currentRoute = Route::current()) {
            $language = $currentRoute->parameter('language');

            if (! in_array($language, Config::get('app.available_locales'))) {
                abort(404);
            }
            App::setLocale($language);
        }
    }
}
